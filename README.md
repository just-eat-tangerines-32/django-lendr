# Lendr

A simple loan management app in Django.

This app was previously closed source. But is now released as open-source. A new version of Lendr has been borne from this project, which is at the time closed-source.

Featurs include:

- Banks and Accounts
- Client Registration
- Simple Dashboard
- Double-entry accounting principles
- Loans and Payments

# Concept

## Banks

Banks hold money and a currency. Banks can **deposit**, **widthraw**, or **transfer** funds between different Bank accounts. Each transaction is recorded and logged.


## Clients

Clients can only one belong to one bank! Clients are able to take out **Loans** from the bank.

## Loans

Loans can be created by a Client. The Client's bank is where the funds will come from. 

### Creating a loan

These are the required paramaters for creating a loan:

* **Principal**: The principal amount of the loan (1000)
* **interest_rate**: The interest of the loan (5.5 == 5.5%)
* **term**: The loans term in months (3 months)

These are additional parameters, with sane defaults

* **late_fee**: The late fee of the loan
* **grace_period**: The number of days before a payment is considered late

Loan's are initially in a **Waiting** state, until released.

Loans can be released in the future.

Loans can generate an **amortization schedule** with fixed payments. Payments can be added or modified or marked paid or late as needed.


### Approving Loans

When the Loan is approved and the funds are released to the client,the Loan's can be **Released**. Only the Admin can release loans.


When the loan is **Released**, a payment schedule is generated

### Paying Loans

The Payment Schedule generates the schedule of payments. 

When the client makes a payment the Admin can record it.

This means the client has paid for the upcoming payment with the **amount due**.


### Advance Payments

Loan payments can be paid in full at anytime. When a **Full payment** is made the loan is considered **Closed**.


### Late Payments

Late Payments are payments that have not been paid within the loan's grace period. The Admin can see all the late payments in the dashboard.

The Admin can mark the Payment as late. A late fee is applied for the marked Payment.

## Dashboard

The dashboard is where Admins can see relevant information such as:

* Banks and their current funds
* 5 Most recent transactiosn from the Bank
* 5 Upcoming Payments
* 5 Late Payments
* 5 Latest Clients

## NavBar

The Navbar consists of

* Log In/Out
* Clients: A shortcut to all the clients
* Loans: A shortcut to all the loans
* Banks: A shortcut to all the banks
* Dashboard: Go back to the Dashboard
* Late: Shortcut to all late loans


# Running the app

Ideally, this app should be run under gunicorn or a uwsgi manager.

But in simple cases, just install the requirements and run the app.

This app requires a Postgresql databse. Database credentials should be input in the ```config/settings/local.py``` file.


```
pip install -r requirements.txt
python manage.py createsuperuser
python manage.py runserver
```







