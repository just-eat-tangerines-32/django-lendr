import datetime
from decimal import Decimal

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import CreateView, DetailView
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse

from lendr.loans.models import Loan, Payment
from lendr.clients.models import Client
from lendr.banks.models import Bank
from .forms import LoanCalculatorForm

# Create your views here.


def CalculatorView(request):
    form = LoanCalculatorForm(request.POST or None)
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            principal = form.cleaned_data['principal']
            interest_rate = form.cleaned_data['interest_rate']
            schedule = form.cleaned_data['schedule']
            term = form.cleaned_data['term']
            applyProceeds = form.cleaned_data['applyProceeds']
            loan = Loan()
            loan.principal = Decimal(principal)
            loan.interest_rate = Decimal(interest_rate)
            loan.schedule = schedule
            loan.term = Decimal(term)
            loan.applyProceeds = applyProceeds
            data["loan"] = loan
            context = {'form': form, 'data': data}
            return render(request, "calculator/loan_form.html", context)

    # if a GET (or any other method) we'll create a blank form
    context = {'form': form, 'data': data}
    return render(request, "calculator/loan_form.html", context)


####### Use this for the actual Loan Creation ########
class OldCalculatorView(CreateView):
    model = Loan
    fields = ['principal', 'interest_rate',
              'schedule', 'term', 'applyProceeds']

    def form_valid(self, form):
        test_bank = Bank.objects.get_or_create(
            name="Test", balance=Decimal(100), currency="CAD")
        test_client, created = Client.objects.get_or_create(
            first_name="Robot", last_name="Test", branch=Bank.objects.first())
        form.instance.borrower = test_client
        return super(CalculatorView, self).form_valid(form)


class CalculatorDetail(DetailView):
    model = Loan

    def get_object(self):
        loan = get_object_or_404(Loan, uuid=self.kwargs['uuid'])
        loan.release()
        return loan

    def get_context_data(self, **kwargs):
        context = super(CalculatorDetail, self).get_context_data(**kwargs)
        payments = Payment.objects.filter(loan_id=self.object)
        context['payments'] = payments
        return context
