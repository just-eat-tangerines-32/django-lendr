from django import forms
from lendr.loans.models import Loan


class LoanCalculatorForm(forms.Form):
    principal = forms.DecimalField(
        label='Principal', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 10000'}))
    interest_rate = forms.DecimalField(
        label='Interest Rate', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 2.5'}))
    term = forms.IntegerField(
        label='Term', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 6'}))
    schedule = forms.ChoiceField(
        label='Schedule', required=True, choices=[x.value for x in Loan.SCHEDULE])
    applyProceeds = forms.BooleanField(label='Apply Proceeds', required=False)
