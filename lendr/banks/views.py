from django.shortcuts import render, get_object_or_404
from django.views.generic import DetailView, UpdateView, CreateView
from django.views.generic.list import ListView
from lendr.utils import GetBankClients, GetBankLoans, GetBankPayments, GetTotalBalance
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.edit import ModelFormMixin


from .models import Bank, Transaction
from lendr.clients.models import Client
from .forms import BankDepositForm, BankTransferForm, NewBankClientForm, NewBankForm


# Create your views here.
class CreateBankView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = "banks.add_bank"
    raise_exception = True
    model = Bank
    template_name = "banks/new.html"
    form_class = NewBankForm


class BankRenameView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = "banks.change_bank"
    raise_exception = True
    model = Bank
    fields = ['name']
    template_name_suffix = '_rename_form'

    def get_object(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return bank


class AllBanksView(ListView):

    model = Bank
    template_name = 'banks/all.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(AllBanksView, self).get_context_data(**kwargs)
        context['total_balance'] = GetTotalBalance()
        return context


class BankPaymentsView(ListView):

    template_name = 'banks/payments_list.html'
    paginate_by = 25

    def get_queryset(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return GetBankPayments(bank.uuid).filter(paid=False)

    def get_context_data(self, **kwargs):
        context = super(BankPaymentsView, self).get_context_data(**kwargs)
        branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        context['bank'] = branch
        context['payments'] = GetBankPayments(
            branch.uuid).filter(paid=False).count()
        return context


class BankLoansView(LoginRequiredMixin, ListView):

    template_name = 'banks/loans_list.html'
    paginate_by = 25

    def get_queryset(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return GetBankLoans(bank.uuid)

    def get_context_data(self, **kwargs):
        context = super(BankLoansView, self).get_context_data(**kwargs)
        branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        context['bank'] = branch
        context['loans'] = GetBankLoans(branch.uuid).count()
        return context


class BankClientsView(LoginRequiredMixin, ListView):

    template_name = 'banks/clients_list.html'
    paginate_by = 25

    def get_queryset(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return GetBankClients(bank.uuid)

    def get_context_data(self, **kwargs):
        context = super(BankClientsView, self).get_context_data(**kwargs)
        branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        context['bank'] = branch
        context['clients'] = GetBankClients(branch.uuid).count()
        return context


class BankTransactionsView(LoginRequiredMixin, ListView):

    template_name = 'banks/transactions_list.html'
    paginate_by = 25

    def get_queryset(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return bank.transactions()

    def get_context_data(self, **kwargs):
        context = super(BankTransactionsView, self).get_context_data(**kwargs)
        branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        context['bank'] = branch
        return context


class BankNewClientView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = ("clients.add_client")
    raise_exception = True

    model = Client
    template_name = "banks/new_client.html"
    form_class = NewBankClientForm

    def get_success_url(self):
        return reverse('clients:client_details', kwargs={
            'uuid': self.object.uuid, 'tab': 'loan'})

    def form_valid(self, form):
        form.instance.branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        msg = "Client: {} {} was succesfully registered!".format(
            form.instance.first_name, form.instance.last_name)
        messages.success(self.request, msg)
        return super(BankNewClientView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BankNewClientView, self).get_context_data(**kwargs)
        branch = Bank.objects.get(uuid=self.kwargs['uuid'])
        context['bank'] = branch
        return context


@login_required(redirect_field_name='DashRedirect')
@permission_required('banks.change_bank', raise_exception=True)
def BankDepositView(request, uuid):

    bank = get_object_or_404(Bank, uuid=uuid)
    form = BankDepositForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            amount = form.cleaned_data['amount']
            reason = form.cleaned_data['reason']
            print(reason)
            bank.deposit(amount, reason)
            msg = "{} {} was succesfully deposited".format(
                amount, bank.currency)
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('banks:bank_details', kwargs={'uuid': bank.uuid}))

    # if a GET (or any other method) we'll create a blank form
    context = {'form': form, 'bank': bank}
    return render(request, "banks/deposit.html", context)


@login_required(redirect_field_name='DashRedirect')
@permission_required('banks.change_bank', raise_exception=True)
def BankWidthrawView(request, uuid):

    bank = get_object_or_404(Bank, uuid=uuid)
    form = BankDepositForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            amount = form.cleaned_data['amount']
            reason = form.cleaned_data['reason']
            try:
                bank.widthraw(amount, reason)
            except ValueError:
                msg = Bank.InsufficientFundsMessage
                messages.error(request, msg)
                return HttpResponseRedirect(reverse('banks:bank_widthraw', kwargs={'uuid': bank.uuid}))
            msg = "{} {} was succesfully widthrawn".format(
                amount, bank.currency)
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('banks:bank_details', kwargs={'uuid': bank.uuid}))

    # if a GET (or any other method) we'll create a blank form
    context = {'form': form, 'bank': bank}
    return render(request, "banks/widthraw.html", context)


@login_required(redirect_field_name='DashRedirect')
@permission_required('banks.change_bank', raise_exception=True)
def BankTransferView(request, uuid):

    bank = get_object_or_404(Bank, uuid=uuid)
    form = BankTransferForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            amount = form.cleaned_data['amount']
            destination_bank = form.cleaned_data['banks']
            reason = form.cleaned_data['reason']
            if destination_bank == bank.name:
                msg = "Cannot transfer to the same bank"
                messages.error(request, msg)
                return HttpResponseRedirect(reverse('banks:bank_transfer', kwargs={'uuid': bank.uuid}))
            else:
                destination_bank = Bank.objects.get(name=destination_bank)
            try:
                bank.transfer(amount, destination_bank, reason)
            except ValueError:
                msg = Bank.InsufficientFundsMessage
                messages.error(request, msg)
                return HttpResponseRedirect(reverse('banks:bank_transfer', kwargs={'uuid': bank.uuid}))
            msg = "{} {} was succesfully transferred to {}".format(
                amount, bank.currency, destination_bank.name)
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('banks:bank_details', kwargs={'uuid': bank.uuid}))

    # if a GET (or any other method) we'll create a blank form
    context = {'form': form, 'bank': bank}
    return render(request, "banks/transfer.html", context)


class BankDetailView(LoginRequiredMixin, DetailView):
    raise_exception = True
    model = Bank
    redirect_field_name = 'DashRedirect'

    def get_object(self):
        bank = get_object_or_404(Bank, uuid=self.kwargs['uuid'])
        return bank

    def get_context_data(self, **kwargs):
        context = super(BankDetailView, self).get_context_data(**kwargs)
        clients = GetBankClients(self.object.uuid)
        loans = GetBankLoans(self.object.uuid)
        payments = GetBankPayments(self.object.uuid)
        released_loans = loans.filter(status='released')
        context['clients_count'] = clients.count()
        context['clients'] = clients[:10]
        context['loans_count'] = loans.count()
        context['loans'] = loans[:10]
        context['payments'] = payments.filter(paid=False)[:10]
        context['released_loans_count'] = released_loans.count()
        context['transactions'] = self.object.transactions()[:15]
        return context
