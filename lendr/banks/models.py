from decimal import Decimal

from lendr.utils import id_generator
from enum import Enum

from django.db import models
from django.db.models import Q


# Create your models here.


class Bank(models.Model):

    class CURRENCY(Enum):
        php = ('PHP', 'PHP')
        cad = ('CAD', 'CAD')
        usd = ('USD', 'USD')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    InsufficientFundsMessage = "Not enough balance to perform the transaction"

    name = models.CharField(max_length=32, blank=False,
                            null=False, unique=True)
    balance = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal(0))
    uuid = models.CharField(max_length=22,
                            db_index=True, default=id_generator, editable=False)

    currency = models.CharField(
        max_length=3, choices=[x.value for x in CURRENCY], default="CAD")

    def add_money(self, money):
        """ Add money into this bank from thin air """
        self.balance = self.balance + money
        self.save(update_fields=['balance'])

    def remove_money(self, money):
        """ Remove money from bank balance """
        self.balance = self.balance - money
        self.save(update_fields=['balance'])

    def has_sufficient_funds(self, money):
        if self.balance >= money:
            return True
        return False

    def widthraw(self, money, reason):
        """ Remove money from self """
        if self.has_sufficient_funds(money):
            self.remove_money(money)
            Transaction.objects.create(
                source_bank=self, destination_bank=self, amount=money, action_type=Transaction.ACTIONS.get_value('widthraw'), reason=reason)
        else:
            raise ValueError(Bank.InsufficientFundsMessage)

    def deposit(self, money, reason):
        """ Deposit money to self """
        self.add_money(money)
        Transaction.objects.create(
            source_bank=self, destination_bank=self, amount=money, action_type=Transaction.ACTIONS.get_value('deposit'), reason=reason)

    def transfer(self, money, destination, reason):
        """ Transfer funds to another bank """
        if self.has_sufficient_funds(money):
            self.remove_money(money)
            destination.add_money(money)
            Transaction.objects.create(
                source_bank=self, destination_bank=destination, amount=money, action_type=Transaction.ACTIONS.get_value('transfer'), reason=reason)
        else:
            raise ValueError(Bank.InsufficientFundsMessage)

    def release(self, money, destination):
        """ Release funds to client """
        if self.has_sufficient_funds(money):
            self.remove_money(money)
            Transaction.objects.create(
                source_bank=self, destination_client=destination, amount=money, action_type=Transaction.ACTIONS.get_value('release'))
        else:
            raise ValueError(Bank.InsufficientFundsMessage)

    def pay(self, money, source):
        """ Recieve a payment from a client """
        self.add_money(money)
        Transaction.objects.create(
            source_client=source, destination_bank=self, amount=money, action_type=Transaction.ACTIONS.get_value('payment'))

    def unpay(self, money, source):
        """ Recieve a payment from a client """
        self.remove_money(money)
        Transaction.objects.create(
            source_bank=self, destination_client=source, amount=money, action_type=Transaction.ACTIONS.get_value('unpay'))

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('banks:bank_details', kwargs={'uuid': self.uuid})

    def transactions(self):
        return Transaction.objects.filter(Q(source_bank__uuid=self.uuid) | Q(destination_bank__uuid=self.uuid))

    def __str__(self):
        return "{}:{}".format(self.name, self.balance)

    def __unicode__(self):
        return


class Transaction(models.Model):
    """Transactions keep track of Bank activity """
    class ACTIONS(Enum):
        deposit = ('deposit', 'Deposit')
        payment = ('payment', 'Payment')
        unpay = ('unpay', 'Unpay')
        widthraw = ('widthraw', 'Widthraw')
        transfer = ('transfer', 'Transfer')
        release = ('release', 'Release')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    timestamp = models.DateTimeField(auto_now=True)
    uuid = models.CharField(max_length=22,
                            db_index=True, default=id_generator, editable=False)
    source_bank = models.ForeignKey(
        Bank, related_name='source_bank', on_delete=models.CASCADE, default=None, blank=True, null=True)
    destination_bank = models.ForeignKey(
        Bank, related_name='destination_bank', on_delete=models.CASCADE, default=None, blank=True, null=True)
    destination_client = models.CharField(
        max_length=300, default="", blank=True)
    source_client = models.CharField(
        max_length=300, default="", blank=True)
    amount = models.DecimalField(
        max_digits=10, decimal_places=2)
    action_type = models.CharField(
        max_length=8, choices=[x.value for x in ACTIONS])
    reason = models.CharField(max_length=320, default="", null=True, blank=True)

    def __str__(self):
        return

    def __unicode__(self):
        return

    class Meta:
        ordering = ["-timestamp"]
