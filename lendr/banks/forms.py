from django import forms
from django.forms import ModelForm

from lendr.loans.models import Loan
from lendr.clients.models import Client
from .models import Bank


class NewBankClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'input'}),
            'last_name': forms.TextInput(attrs={'class': 'input'}),
        }


class NewBankForm(ModelForm):
    class Meta:
        model = Bank
        fields = ['name', 'currency', 'balance']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'input'}),
            'balance': forms.TextInput(attrs={'class': 'input'}),
        }


class BankDepositForm(forms.Form):
    amount = forms.DecimalField(
        label='Amount', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 10000'}))
    reason = forms.CharField(
        label='Comment', widget=forms.TextInput(attrs={'class': 'input'}))


class BankTransferForm(forms.Form):
    CHOICES = []

    # for bank in Bank.objects.all():
    #     out = (bank.name, bank.name)
    #     CHOICES.append(out)

    amount = forms.DecimalField(
        label='Amount', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 10000'}))
    banks = forms.ChoiceField(
        label='Banks', required=True, choices=CHOICES)
    reason = forms.CharField(
        label='Comment', widget=forms.TextInput(attrs={'class': 'input'}))
