from django import template

register = template.Library()


@register.filter(name="pretty_money")
def prettymoney(value, arg):
    if arg:
        return "{:,} {}".format(value, arg)
    return "{:,}".format(value)
