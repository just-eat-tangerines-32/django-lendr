from decimal import Decimal

from django.test import TestCase
from django.db.models import Q

from .models import Bank
from .models import Transaction
# Create your tests here.


class BankTests(TestCase):

    def setUp(self):
        Bank.objects.get_or_create(name="Toronto", balance=Decimal(0))
        Bank.objects.get_or_create(name="Montreal", balance=Decimal(100))

        Bank.objects.get_or_create(name="Etobicoke", balance=Decimal(0))

    def test_objects_created(self):
        amt = Bank.objects.all().count()
        self.assertEqual(amt, 3)

    def test_id_and_uuid(self):
        amt = Bank.objects.all().first()
        self.assertNotEqual(amt.id, None)
        self.assertNotEqual(amt.uuid, None)

    def test_initial_amount(self):
        amt = Bank.objects.all().first()
        self.assertEqual(amt.balance, Decimal(0))

    def test_add_money(self):
        b = Bank.objects.all().first()
        amount = Decimal(100.5)
        expected_balance = b.balance - amount
        b.remove_money(Decimal(100.5))
        self.assertEqual(b.balance, expected_balance)

    def test_remove_money(self):
        b = Bank.objects.all().all()[1]
        amount = Decimal(100.5)
        expected_balance = b.balance - amount
        b.remove_money(Decimal(100.5))
        self.assertEqual(b.balance, expected_balance)

    def test_deposit_money(self):
        b = Bank.objects.all().first()
        amount = Decimal(100.5)
        expected_balance = b.balance + amount
        b.deposit(amount)
        self.assertEqual(b.balance, expected_balance)
        tx = Transaction.objects.all()
        self.assertEqual(tx.count(), 1)

    def test_widthraw_money(self):
        b = Bank.objects.all()[1]
        amount = Decimal(5.00)
        expected_balance = b.balance - amount
        b.widthraw(amount)
        self.assertEqual(b.balance, expected_balance)
        tx = Transaction.objects.all()
        self.assertEqual(tx.count(), 1)

    def test_fail_widthraw_no_balance(self):
        b = Bank.objects.first()
        amount = Decimal(5.00)
        try:
            b.widthraw(amount)
        except ValueError:
            pass
        else:
            self.fail("Able to widthraw without balance")

    def test_transfer_money(self):
        to = Bank.objects.all().first()
        mt = Bank.objects.all()[1]
        expected_balance = mt.balance - Decimal(1.50)
        expected_balance2 = to.balance + Decimal(1.50)
        mt.transfer(Decimal(1.50), to)

        self.assertEqual(to.balance, expected_balance2)
        self.assertEqual(mt.balance, expected_balance)

        tx = Transaction.objects.all()
        self.assertEqual(tx.count(), 1)

    def test_get_bank_transactions(self):
        to = Bank.objects.all().first()
        mt = Bank.objects.all()[1]

        amount = Decimal('500.00')
        to.deposit(amount)
        mt.deposit(amount)
        mt.transfer(amount, to)
        self.assertEqual(to.transactions().count(), 2)
        self.assertEqual(mt.transactions().count(), 2)
