from django import forms


class DateTypeInput(forms.DateInput):
    input_type = 'date'

class AddPaymentForm(forms.ModelForm):
    class Meta:
        from .models import Payment
        model = Payment
        fields = ['amount_due', 'due_date']
        widgets = {
            'due_date': DateTypeInput(),
        }

class UpdatePaymentForm(forms.ModelForm):
    class Meta:
        from .models import Payment
        model = Payment
        fields = ['amount_due', 'due_date', 'paid_date']
        widgets = {
            'due_date': DateTypeInput(),
            'paid_date': DateTypeInput(),
        }

class DatePickerForm(forms.Form):
    start_date = forms.DateField(label='Start Date', widget=DateTypeInput())
    end_date = forms.DateField(label='End Date', widget=DateTypeInput())


class PayForm(forms.Form):
    amount = forms.DecimalField(
        label='Amount', required=True, widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'e.g. 10000'}))


class ReleasePickerForm(forms.Form):
    release_date = forms.DateField(
        label='Release Date', widget=DateTypeInput())

class NoteForm(forms.Form):
    note = forms.Textarea()