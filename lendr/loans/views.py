import datetime

from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import DetailView
from django.shortcuts import render, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.db import transaction
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin


from .models import Loan, Payment
from .forms import DatePickerForm, ReleasePickerForm, PayForm, AddPaymentForm, UpdatePaymentForm
from lendr.banks.models import Bank
from lendr.utils import GetLoanPayments

# Create your views here.
class DeletePayment(PermissionRequiredMixin, DeleteView):
    permission_required = "loans.delete_loan"
    raise_exception = True
    model = Payment
    template_name = "loans/loan_delete_payment.html"

    def get_object(self):
        payment = get_object_or_404(Payment, uuid=self.kwargs['pay_uuid'])
        return payment

    def get_success_url(self):
        msg = "Payment was succesfully deleted!"
        messages.success(self.request, msg)
        return reverse('loans:loan_details', kwargs={
            'uuid': self.kwargs['uuid']})

class UpdatePayment(PermissionRequiredMixin, UpdateView):
    permission_required = "loans.change_payment"
    raise_exception = True
    model = Payment
    form_class = UpdatePaymentForm
    template_name = "loans/loan_update_payment.html"

    def get_object(self):
        payment = get_object_or_404(Payment, uuid=self.kwargs['pay_uuid'])
        return payment

    def get_success_url(self):
        return reverse('loans:loan_details', kwargs={
            'uuid': self.kwargs['uuid']})

    def get_context_data(self, **kwargs):
        context = super(UpdatePayment, self).get_context_data(**kwargs)
        loan = get_object_or_404(Loan, uuid=self.kwargs['uuid'])
        payment = get_object_or_404(Payment, uuid=self.kwargs['pay_uuid'])
        context['object'] = loan
        context['payment'] = payment
        return context

    def form_valid(self, form):
        form.instance.loan_id = Loan.objects.get(uuid=self.kwargs['uuid'])
        msg = "Payment was succesfully updated!"
        messages.success(self.request, msg)
        return super(UpdatePayment, self).form_valid(form)

class AddPayment(PermissionRequiredMixin, CreateView):
    permission_required = ("loans.add_payment")
    raise_exception = True
    model = Payment
    form_class = AddPaymentForm
    template_name = "loans/loan_new_payment.html"

    def get_success_url(self):
        return reverse('loans:loan_details', kwargs={
            'uuid': self.kwargs['uuid']})

    def get_context_data(self, **kwargs):
        context = super(AddPayment, self).get_context_data(**kwargs)
        loan = get_object_or_404(Loan, uuid=self.kwargs['uuid'])
        context['object'] = loan
        return context


    def form_valid(self, form):
        form.instance.loan_id = Loan.objects.get(uuid=self.kwargs['uuid'])
        msg = "Payment was succesfully added!"
        messages.success(self.request, msg)
        return super(AddPayment, self).form_valid(form)


@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def UnpayOutstandingBalance(request, uuid):

    loan = get_object_or_404(Loan, uuid=uuid)
    form = PayForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            amount = form.cleaned_data['amount']
            loan.unpay_outstanding_balance(amount)
            msg = "{} {} was succesfully unpaid".format(
                amount, loan.borrower.branch.currency)
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))

    context = {'form': form, 'loan': loan}
    return render(request, "loans/unpay_outstanding.html", context)


@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def PayOutstandingBalance(request, uuid):

    loan = get_object_or_404(Loan, uuid=uuid)
    form = PayForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            amount = form.cleaned_data['amount']
            loan.pay_outstanding_balance(amount)
            msg = "{} {} was succesfully paid".format(
                amount, loan.borrower.branch.currency)
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))

    context = {'form': form, 'loan': loan}
    return render(request, "loans/pay_outstanding.html", context)


@login_required()
def QueryPayments(request):
    form = DatePickerForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            start = str(form.cleaned_data['start_date'])
            end = str(form.cleaned_data['end_date'])
            context = {'start_date': start, 'end_date': end}
            return HttpResponseRedirect(reverse('loans:custom_query_payments', kwargs=context))
    return render(request, 'loans/query.html', {'form': form})


class CustomPaymentsViewer(LoginRequiredMixin, ListView):

    model = Payment
    template_name = 'loans/payments_list.html'
    paginate_by = 25

    def get_queryset(self):
        start = self.kwargs['start_date']
        end = self.kwargs['end_date']

        # compare the datetime
        # start must be less than end

        start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d')

        if start_date > end_date:
            messages.error('Start date must be greater than end date')
            return HttpResponseRedirect(reverse('dashboard'))

        wanted_items = set()
        candidates = Payment.objects.filter(
            due_date__gte=start_date, due_date__lte=end_date)
        for item in candidates:
            if not item.paid:
                wanted_items.add(item.pk)

        return Payment.objects.filter(pk__in=wanted_items)

    def get_context_data(self, **kwargs):
        context = super(CustomPaymentsViewer, self).get_context_data(**kwargs)
        start = self.kwargs['start_date']
        end = self.kwargs['end_date']
        start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d')
        message = "Payments due from {} to {}".format(
            start_date.date(), end_date.date())
        context['message'] = message
        return context


class LoanDelete(PermissionRequiredMixin, SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    permission_required = "loans.delete_loan"
    raise_exception = True
    redirect_field_name = 'DashRedirect'
    model = Loan
    template_name = 'loans/delete_confirm.html'
    success_message = 'Loan successfully deleted!'

    def get_success_url(self):
        client = self.object.borrower.uuid
        return reverse_lazy('clients:client_details', kwargs={'uuid': client, 'tab': 'loan'})

    def get_object(self):
        loan = get_object_or_404(Loan, uuid=self.kwargs['uuid'])
        return loan

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(LoanDelete, self).delete(request, *args, **kwargs)


class AllLatePaymentsView(LoginRequiredMixin, ListView):

    model = Payment
    template_name = 'loans/payments_list.html'
    paginate_by = 25

    def get_queryset(self):
        wanted_items = set()
        for item in Payment.objects.all():
            if item.is_late() and not item.paid:
                wanted_items.add(item.pk)

        return Payment.objects.filter(pk__in=wanted_items)

    def get_context_data(self, **kwargs):
        context = super(AllLatePaymentsView, self).get_context_data(**kwargs)
        message = "Late"
        context['message'] = message
        return context


class ReleasedLoansList(LoginRequiredMixin, ListView):

    model = Loan
    template_name = 'loans/all.html'
    paginate_by = 25

    def get_queryset(self):
        wanted_items = set()
        for item in Loan.objects.all():
            if item.status == "released":
                wanted_items.add(item.pk)

        return Loan.objects.filter(pk__in=wanted_items)


class CloseLoansList(LoginRequiredMixin, ListView):

    model = Loan
    template_name = 'loans/all.html'
    paginate_by = 25

    def get_queryset(self):
        wanted_items = set()
        for item in Loan.objects.all():
            if item.status == "closed":
                wanted_items.add(item.pk)

        return Loan.objects.filter(pk__in=wanted_items)


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def ApplyInterest(request, uuid, pay_uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    payment = get_object_or_404(Payment, uuid=pay_uuid)
    if payment.is_late() == False:
        messages.error(
            request, "Payment {} is not late".format(payment.uuid))
    else:
        old = payment.amount_due
        payment.apply_interest()
        new = payment.amount_due
        messages.success(
            request, "Interest was applied to Payment {}. Old due: {}, New due: {}  ".format(payment.uuid, old, new))

    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def MarkLate(request, uuid, pay_uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    payment = get_object_or_404(Payment, uuid=pay_uuid)
    if payment.is_late() == False:
        messages.error(
            request, "Payment {} is not late".format(payment.uuid))

    if payment.late_fee_applied == True:
        messages.error(
            request, "Payment {} already has a late fee applied".format(payment.uuid))
    else:
        old = payment.amount_due
        payment.mark_late()
        new = payment.amount_due
        messages.success(
            request, "Late fee was applied to Payment {}. Old due: {}, New due: {}  ".format(payment.uuid, old, new))

    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def Unpay(request, uuid, pay_uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    payment = get_object_or_404(Payment, uuid=pay_uuid)
    if payment.paid == False:
        messages.error(
            request, "Payment {} is already unpaid".format(payment.uuid))
    else:
        payment.unpay()
        messages.success(
            request, "Payment {} was succesfully unpaid".format(payment.uuid))

    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def Pay(request, uuid, pay_uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    payment = get_object_or_404(Payment, uuid=pay_uuid)
    if payment.paid == True:
        messages.error(
            request, "Payment {} is already paid".format(payment.uuid))
    else:
        payment.pay()
        messages.success(
            request, "Payment {} was succesfully paid".format(payment.uuid))

    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def FullPaymentLoan(request, uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'closed':
        messages.error(request, "This loan is already closed")
    else:
        loan.full_payment()
        messages.success(
            request, "The loan was succesfully closed. The full payment was recieved")
    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def FullPaymentConfirm(request, uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'closed':
        messages.error(request, "This loan is already closed")
        return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))
    else:
        context = {'loan': loan}
        return render(request, "loans/full_confirm.html", context)


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def CloseLoan(request, uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'closed':
        messages.error(request, "This loan is already closed")
    else:
        loan.close()
        messages.success(request, "The loan was succesfully closed")
    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def CloseLoanConfirm(request, uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'closed':
        messages.error(request, "This loan is already closed")
        return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))
    else:
        context = {'loan': loan}
        return render(request, "loans/close_confirm.html", context)


@transaction.atomic
@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def ReleaseLoan(request, uuid, date):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'released':
        messages.error(request, "This loan is already released")
    else:
        try:
            release_date = datetime.datetime.strptime(
                date, '%Y-%m-%d')
            loan.release(date=release_date)
        except ValueError:
            messages.error(request, Bank.InsufficientFundsMessage)
            return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))
        messages.success(request, "The loan was succesfully released")
    return HttpResponseRedirect(reverse('loans:loan_details', kwargs={'uuid': loan.uuid}))


@login_required(redirect_field_name='DashRedirect')
@permission_required('loans.change_loan', raise_exception=True)
def ReleaseLoanConfirm(request, uuid):
    loan = get_object_or_404(Loan, uuid=uuid)
    if loan.status == 'released':
        messages.error(request, "This loan is already released")
    else:
        form = ReleasePickerForm(request.POST or None)
        context = {'loan': loan, 'form': form}
        if request.method == 'POST':
            if form.is_valid():
                release_date = str(form.cleaned_data['release_date'])
                context = {'date': release_date, 'uuid': loan.uuid}
                return HttpResponseRedirect(reverse('loans:loan_release', kwargs=context))
    return render(request, "loans/release_confirm.html", context)


class AllLoansView(LoginRequiredMixin, ListView):

    model = Loan
    template_name = 'loans/all.html'
    paginate_by = 25


class LoanDetailView(LoginRequiredMixin, DetailView):

    model = Loan
    redirect_field_name = 'DashRedirect'

    def get_object(self):
        loan = get_object_or_404(Loan, uuid=self.kwargs['uuid'])
        return loan

    def get_context_data(self, **kwargs):
        context = super(LoanDetailView, self).get_context_data(**kwargs)
        payments = GetLoanPayments(self.object.uuid)
        context['payments'] = payments
        return context
