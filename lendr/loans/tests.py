from decimal import Decimal
import datetime

from django.test import TestCase

from lendr.banks.models import Bank
from lendr.clients.models import Client
from .models import Loan, Payment
# Create your tests here.


class LoanTests(TestCase):

    def setUp(self):
        Bank.objects.create(
            name="Montreal", balance=Decimal(100000), currency="CAD")
        Client.objects.create(
            first_name="Adam", last_name="Brown", branch=Bank.objects.first())
        self.weekly_interest_only_loan = Loan.objects.create(borrower=Client.objects.first(),
                                                             principal=Decimal(
                                                                 10000),
                                                             interest_rate=Decimal(
            "5"),
            schedule=Loan.SCHEDULE.get_value(
            'weekly'),
            term=Decimal("0"),
            payment_method=Loan.PAYMENT_METHOD.get_value('interest_only'),
            applyProceeds=False)
        self.weekly_compound_interest_loan = Loan.objects.create(borrower=Client.objects.first(),
                                                                 principal=Decimal(
                                                                 10000),
                                                                 interest_rate=Decimal(
            "5"),
            schedule=Loan.SCHEDULE.get_value(
            'weekly'),
            term=Decimal("6"),
            payment_method=Loan.PAYMENT_METHOD.get_value('compound_interest'),
            applyProceeds=False)
        self.monthly_late = Loan.objects.create(borrower=Client.objects.first(),
                                                principal=Decimal(
            1000),
            interest_rate=Decimal(
            "15"),
            schedule=Loan.SCHEDULE.get_value(
            'monthly'),
            term=Decimal("3"),
            payment_method=Loan.PAYMENT_METHOD.get_value('compound_interest'),
            applyProceeds=False)
        self.monthly = Loan.objects.create(borrower=Client.objects.first(),
                                           principal=Decimal(10000),
                                           interest_rate=Decimal("5"),
                                           schedule=Loan.SCHEDULE.get_value(
            'monthly'),
            term=Decimal("6"),
            applyProceeds=False)
        Loan.objects.create(borrower=Client.objects.first(),
                            principal=Decimal(1000),
                            interest_rate=Decimal("15"),
                            schedule=Loan.SCHEDULE.get_value('monthly'),
                            term=Decimal('90'),
                            applyProceeds=True)
        self.daily = Loan.objects.create(borrower=Client.objects.first(),
                                         principal=Decimal(10000),
                                         interest_rate=Decimal("10"),
                                         schedule=Loan.SCHEDULE.get_value(
                                             'daily'),
                                         term=Decimal('2'),
                                         applyProceeds=False)

    def test_monthly_loan(self):
        c = self.monthly
        self.assertEqual(c.status, "waiting")

        self.assertEqual(c.total_number_of_payments(), Decimal(6))
        self.assertEqual(c.payment_frequency(), 30)
        self.assertEqual(c.total_loan(), Decimal("13000"))
        self.assertEqual(c.interest(), Decimal(500))
        self.assertEqual(c.total_interest(), Decimal(3000))
        self.assertEqual(c.amount_per_payment(), Decimal("2166.66"))

    def test_loan_release(self):
        c = self.monthly
        bank = Bank.objects.first()
        expected = bank.balance - c.proceeds()
        self.assertIsNone(c.released_date)
        c.release()
        self.assertIsNotNone(c.released_date)
        self.assertEqual(c.status, "released")

        # Check if the bank lost money
        bank.refresh_from_db()
        self.assertEqual(expected, bank.balance)

        # Check if payments were generate
        payments = Payment.objects.filter(loan_id=c)
        self.assertEqual(payments.count(), c.total_number_of_payments())

        # Check payments here quickly
        p = payments[0]
        bank.refresh_from_db()
        client = Client.objects.first()
        self.assertEqual(p.bank_currency(), bank.currency)
        self.assertEqual(p.client_name(), client.full_name())

        # Check the expected amount
        self.assertEqual(
            c.expected, c.total_number_of_payments() * c.amount_per_payment())

        # Make a payment
        p.pay()
        self.assertEqual(p.loan_id, c)

        # Check if the gained gained money
        expected = bank.balance + p.amount_due
        bank.refresh_from_db()
        self.assertEqual(expected, bank.balance)

        # The loan object was updated; refresh to get the latest value
        c.refresh_from_db()

        self.assertEqual(p.loan_id.collected, c.collected)
        self.assertEqual(p.paid, True)
        self.assertEqual(c.collected, p.amount_due)

        # Undo that payment
        bank.refresh_from_db()
        p.unpay()
        c.refresh_from_db()

        # Check if the gained gained money
        expected = bank.balance - p.amount_due
        bank.refresh_from_db()
        self.assertEqual(expected, bank.balance)

        self.assertEqual(p.loan_id.collected, Decimal(0))
        self.assertEqual(p.paid, False)
        self.assertEqual(c.collected, Decimal(0))

        # Get payments for a given bank
        bank_payments = Payment.objects.filter(
            loan_id__borrower__branch__uuid=bank.uuid)
        self.assertEqual(len(bank_payments), 6)

        # Make a full payment
        c.full_payment()
        self.assertEqual(c.status, Loan.STATUS.get_value('closed'))

        # Let's check the payments
        payments = Payment.objects.filter(loan_id=c)
        for payment in payments:
            self.assertEqual(payment.paid, True)

        # Get loans for a given bank
        # bank.refresh_from_db()
        # loans = Loan.objects.filter(borrower__branch__uuid=bank.uuid)
        # self.assertEqual(len(loans), 4)


    def test_daily_loan(self):
        c = self.daily
        self.assertEqual(c.total_interest(), Decimal('2000'))
        self.assertEqual(c.total_loan(), Decimal('12000'))
        self.assertEqual(c.amount_per_payment(), Decimal('200'))

    def test_weekly_compound_interest_loan(self):
        c = self.weekly_compound_interest_loan
        print(c.interest())
        self.assertEqual(c.payment_method, "compound_interest")
        self.assertEqual(c.total_interest(), Decimal('3000'))
        self.assertEqual(c.proceeds(), Decimal('10000'))
        self.assertEqual(c.total_loan(), Decimal('13000'))
        self.assertEqual(c.amount_per_payment(), Decimal('541.66'))

    def test_weekly_interest_only_loan(self):
        c = self.weekly_interest_only_loan
        self.assertEqual(c.payment_method, "interest_only")
        self.assertEqual(c.proceeds(), Decimal('10000'))
        self.assertEqual(c.total_loan(), Decimal('10000'))
        self.assertEqual(c.amount_per_payment(), Decimal('500'))

        # Let relese the loan
        print("Test: Releasing loan")
        c.release()
        # Only the first payments should be generated
        payments = Payment.objects.filter(loan_id=c)
        payment1 = payments[0]
        self.assertEqual(len(payments), 1)

        # Check the outstanding balance
        self.assertEqual(c.outstanding_balance, c.proceeds())

        # Check the first payment due date
        expected_date = datetime.datetime.now().date(
        ) + datetime.timedelta(days=int(c.payment_frequency()))
        print(payments[0].due_date, expected_date,
              datetime.datetime.now().date())
        self.assertEqual(expected_date, payment1.due_date)

        # Pay the first one
        self.assertEqual(Decimal(0), c.collected)
        payment1.pay()

        # Check the collected
        c.refresh_from_db()
        self.assertEqual(payments[0].amount_due, c.collected)

        # Another payment should have been generated
        payments = Payment.objects.filter(loan_id=c)
        self.assertEqual(len(payments), 2)

        # Check the next  due day
        expected_date = payments[0].paid_date + \
            datetime.timedelta(days=int(c.payment_frequency()))
        self.assertEqual(expected_date, payments[1].due_date)

        # Let's now pay the outstanding balance
        c.pay_outstanding_balance(Decimal("1000"))

        # Check the outstanding balance
        self.assertEqual(c.outstanding_balance, Decimal("9000"))

        # Unpay that balance
        c.unpay_outstanding_balance(Decimal("1000"))
        self.assertEqual(c.outstanding_balance, Decimal("10000"))

    def test_monthly_late_compound_interest(self):
        c = self.monthly_late

        past_date = datetime.datetime.now() - datetime.timedelta(days=500)
        c.release(date=past_date.date())

        payments = Payment.objects.filter(loan_id=c)
        print(len(payments))
        first_payment = payments[0]
        self.assertTrue(first_payment.is_late())
        self.assertEqual(c.past_due, Decimal(0))
        first_payment.mark_late()
        c.refresh_from_db()
        self.assertEqual(c.past_due, Decimal("555.83"))

        # Mark the second payment late
        second_payment = payments[1]
        self.assertTrue(second_payment.is_late())
        second_payment.mark_late()
        c.refresh_from_db()
        self.assertEqual(c.past_due, Decimal("1195.03"))

        # Mark all the late payments
        mass = payments[2:len(payments)]
        for i in mass:
            i.mark_late()
