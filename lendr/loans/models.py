from enum import Enum
import datetime
from decimal import Decimal, ROUND_UP, ROUND_DOWN
from lendr.utils import id_generator

from django.db import models
from django.urls import reverse
from django.db.models import Q

from lendr.clients.models import Client


class Loan(models.Model):
    """ This is a the loan object """
    class STATUS(Enum):
        waiting = ('waiting', 'Waiting')
        released = ('released', 'Released')
        closed = ('closed', 'Closed')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    class SCHEDULE(Enum):
        weekly = ('weekly', 'Weekly')
        semimonthly = ('semimonthly', 'Semi-monthly')
        monthly = ('monthly', 'Monthly')
        daily = ('daily', 'Daily')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    class PAYMENT_METHOD(Enum):
        interest_only = ('interest_only', 'Interest Only')
        compound_interest = ('compound_interest', 'Compound interest')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    uuid = models.CharField(max_length=22,
                            db_index=True, default=id_generator, editable=False)
    borrower = models.ForeignKey(Client)
    status = models.CharField(
        max_length=32, choices=[x.value for x in STATUS], default=STATUS.get_value('waiting'))

    principal = models.DecimalField(
        max_digits=15, decimal_places=2, default=Decimal(0))
    interest_rate = models.DecimalField(
        max_digits=5, help_text="%", decimal_places=2, default=Decimal(0))
    schedule = models.CharField(
        max_length=32, choices=[x.value for x in SCHEDULE], default=None, blank=True, null=True)
    payment_method = models.CharField(
        max_length=32, choices=[x.value for x in PAYMENT_METHOD], default=PAYMENT_METHOD.get_value('compound_interest'), blank=True, null=True)
    term = models.DecimalField(
        verbose_name="Term", help_text="Months. (30 days = Month)", max_digits=5, decimal_places=0, default=Decimal(0), blank=True, null=True)

    past_due = models.DecimalField(help_text="The previous amount due",
                                   max_digits=10, decimal_places=2, default=Decimal(0))

    grace_period = models.IntegerField(
        help_text="In days", default=3)
    late_fee_rate = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal('2.5'))
    processing_fee = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal('0'))
    insurance_fee = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal('0'))
    notarial_fee = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal('0'))

    created_date = models.DateTimeField(auto_now=True)
    released_date = models.DateField(
        help_text="The Loan's release date", default=None, blank=True, null=True)

    expected = models.DecimalField(help_text="The expected uncollected amount used by equal payment loans",
                                   max_digits=10, decimal_places=2, default=Decimal(0))

    collected = models.DecimalField(help_text="The total collected amount, used by equal payment loans",
                                    max_digits=10, decimal_places=2, default=Decimal(0))

    outstanding_balance = models.DecimalField(help_text="The principal loan amount used by interest-only loans",
                                              max_digits=10, decimal_places=2, default=Decimal(0))

    def pay_outstanding_balance(self, money):
        """ Deduct the amount of the outstanding balance """
        self.outstanding_balance -= money
        self.save(update_fields=['outstanding_balance'])
        self.borrower.branch.pay(
            money, self.borrower.full_name())

    def unpay_outstanding_balance(self, money):
        """ Return the amount to the outstanding balance """
        self.outstanding_balance += money
        self.save(update_fields=['outstanding_balance'])
        self.borrower.branch.unpay(
            money, self.borrower.full_name())

    def get_absolute_url(self):
        return reverse('loans:loan_details', kwargs={'uuid': self.uuid})

    def total_number_of_payments(self):
        """ Returns the total number of payments for the loan """
        return self.payments_per_month() * self.term

    def deducted_fee(self):
        """ Returns the deducted fee """
        return (self.processing_fee + self.notarial_fee + self.insurance_fee)

    def payment_frequency(self):
        """ Returns the number of days until a payment is due """
        if self.schedule == "weekly":
            return Decimal('7')
        elif self.schedule == "daily":
            return Decimal('1')
        elif self.schedule == "semimonthly":
            return Decimal('15')
        elif self.schedule == "monthly":
            return Decimal('30')

    def proceeds(self):
        """
        Returns the proceed amount that is released to the client
        Proceeds = principal - (principal * interest)
        """
        # Subtract processing fee, notarial fee, and insurance fee
        return (self.principal - self.interest()) - (self.insurance_fee + self.notarial_fee + self.processing_fee)
        #return (self.principal) - (self.insurance_fee + self.notarial_fee + self.processing_fee)

    def interest(self):
        """ Returns the interest of principal """
        out = self.principal * (self.interest_rate / Decimal(100))
        return out.quantize(Decimal('1.'), rounding=ROUND_UP)

    def amount_per_payment(self):
        """ Returns the amount due per payment """
        total_loan = self.total_loan()
        print(total_loan, self.total_number_of_payments())
        # int_p = (self.interest_rate / Decimal(100)) * self.principal
        # sub = total_loan - int_p
        out = total_loan / self.total_number_of_payments()
        return out.quantize(Decimal('.01'), rounding=ROUND_DOWN)

    def total_interest(self):
        """ Returns the interest of the entire loan """
        return (self.interest() * self.term)

    def total_loan(self):
        """ Returns the total loan """
        return self.total_interest() + self.principal

    def close(self):
        self.status = self.STATUS.get_value('closed')
        self.save(update_fields=['status'])

    def release(self, date=None):
        """ Release the funds to client. This will generate a payment schedule """
        print("Releasing")
        if self.status != 'released':
            if self.borrower.branch.has_sufficient_funds(self.proceeds()):
                if date is not None:
                    self.released_date = date
                else:
                    self.released_date = datetime.datetime.now().date()
                self.status = self.STATUS.get_value('released')
                self.save(update_fields=['released_date', 'status'])
                print("Release date of loan", self.released_date)

                # Add the outstanding balance if its an interest-only loan
                if self.payment_method == "interest_only":
                    self.outstanding_balance = self.proceeds()
                    self.save(update_fields=['outstanding_balance'])
                # Remove the money from Bank
                self.borrower.branch.release(
                    self.proceeds(), self.borrower.full_name())
                self.generate_payments()
            else:
                raise ValueError(self.borrower.branch.InsufficientFundsMessage)

    def set_past_due(self, money):
        print("Setting past due:", money)
        self.past_due = money
        print("past due:", self.past_due)
        self.save(update_fields=['past_due'])

    def add_to_expected(self, money):
        """ Add money to loan expected field """
        self.expected += money
        self.save(update_fields=['expected'])

    def remove_from_expected(self, money):
        """ Remove money to loan expected field """
        self.expected -= money
        self.save(update_fields=['expected'])

    def payments_per_month(self):
        """ Returns the number of payments per month """
        if self.schedule == 'weekly':
            return Decimal(4)
        elif self.schedule == 'semimonthly':
            return Decimal(2)
        elif self.schedule == 'monthly':
            return Decimal(1)
        elif self.schedule == 'daily':
            return Decimal(30)

    def add_to_collected(self, money):
        """ Add money to loan collected field """
        self.collected += money
        self.save()

    def remove_from_collected(self, money):
        """ Remove money to loan expected field """
        self.collected -= money
        self.save(update_fields=['collected'])

    def generate_payment(self, amount_due, due_date):
        print("due:", due_date)
        Payment.objects.create(
            loan_id=self, amount_due=amount_due, due_date=due_date)

    def add_days(start, days):
        start += datetime.timedelta(days=days)
        return start

    def generate_payments(self):
        payment_frequency = self.payment_frequency()
        num_of_payments = self.total_number_of_payments()
        amount_per_payment = self.amount_per_payment()

        if self.payment_method == "interest_only":
            print("Interst_only: generating payments")
            # If there are outstanding balances
            # Just generate the next payment date
            # Check if there are previous paid payments
            payments = Payment.objects.filter(Q(loan_id=self) & Q(
                paid=True)).order_by("-paid_date")
            print("First payment:", payments)
            if not payments.exists():
                # generate the first payment
                print("generate first payment")
                due_date = Loan.add_days(
                    self.released_date, int(payment_frequency))
                self.generate_payment(amount_per_payment, due_date)
            else:
                print("Past payments have been made; lets take the latest one")
                candidate = payments[0]
                print("Candidate:", candidate)
                print("Candidate paid date:", candidate.paid_date)
                print("Candidates:", payments)
                self.generate_payment(amount_per_payment, Loan.add_days(
                    candidate.paid_date, int(payment_frequency)))
        elif self.payment_method == "compound_interest":
            """ Generate the amortization schedule """
            print("Generating compound interest payments")
            print("Num of payments", num_of_payments)
            for i in enumerate(range(int(num_of_payments)), 1):
                multiplier = int(payment_frequency) * i[0]
                print("Multiploer:", multiplier)
                calculated_date = Loan.add_days(self.released_date, multiplier)
                self.generate_payment(amount_per_payment, calculated_date)
                self.add_to_expected(amount_per_payment)

    def full_payment(self):
        """
            Marks the loan as fully paid. This will change the loan's stattus to CLOSED.
            All payments associated with this loan_id will also be deleted. Be warned. This operation cannot be reversed
        """
        self.close()
        if self.payment_method == "interest_only":
            self.pay_outstanding_balance(self.outstanding_balance)
            self.save()
        else:
            payments = Payment.objects.filter(loan_id__uuid=self.uuid)
            for payment in payments:
                payment.pay()
            self.refresh_from_db()
            self.save()

    def __str__(self):
        return

    def __unicode__(self):
        return

    class Meta:
        ordering = ["-created_date"]


class Payment(models.Model):
    """ This is a payment object """

    uuid = models.CharField(max_length=22,
                            db_index=True, default=id_generator, editable=False)
    loan_id = models.ForeignKey(Loan)
    amount_due = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal(0))
    amount_paid = models.DecimalField(
        max_digits=10, decimal_places=2, default=Decimal(0))
    due_date = models.DateField()
    paid_date = models.DateField(default=None, blank=True, null=True)
    paid = models.NullBooleanField(default=False, blank=True, null=True)
    late_fee_applied = models.NullBooleanField(
        default=False, blank=True, null=True)

    def bank_currency(self):
        return self.loan_id.borrower.branch.currency

    def client_name(self):
        return self.loan_id.borrower.full_name()

    def money(self):
        return "{} {}".format(self.amount_due, self.bank_currency())

    def __str__(self):
        return "{}: {} {}".format(self.uuid, self.due_date, self.money())

    def is_late(self):
        """ Returns true if the payment due date + grace priod days past today """
        if self.due_date + datetime.timedelta(days=self.loan_id.grace_period) < datetime.datetime.now().date():
            return True
        return False

    def apply_interest(self):
        self.amount_due = (self.loan_id.interest_rate / Decimal("100")) * (
            self.amount_due) + self.amount_due
        self.loan_id.set_past_due(self.amount_due)
        self.save(update_fields=['amount_due'])

    def custom_pay(self, money):
        """ Make a Custom Payment """
        pass
        #if self.paid == False:
        #    self.paid = True
        #    self.paid_date = datetime.datetime.now().date()
        #    self.loan_id.add_to_collected(money)
        #    self.loan_id.borrower.branch.pay(
        #        self.money, self.loan_id.borrower.full_name())
        #if self.loan_id.past_due > 0:
        #    self.loan_id.past_due -= self.amount_due
        #    self.loan_id.save()

    def pay(self):
        """ Mark this payment as paid """
        if self.paid == False:

            self.paid = True
            self.paid_date = datetime.datetime.now().date()
            self.loan_id.add_to_collected(self.amount_due)
            self.amount_paid = self.amount_due
            self.loan_id.borrower.branch.pay(
                self.amount_due, self.loan_id.borrower.full_name())
            self.save()
            if self.loan_id.payment_method == "interest_only":
                """ This will only pay the interset and not the outstanding """
                if self.loan_id.outstanding_balance > 0:
                    self.loan_id.generate_payments()
            elif self.loan_id.payment_method == "compound_interest":
                """ This will pay the past due """
                if self.loan_id.past_due > 0:
                    self.loan_id.past_due -= self.amount_due
                    self.loan_id.save()

    def unpay(self):
        """ Revoke this payment """
        if self.paid == True:
            self.paid = False
            self.paid_date = None
            self.loan_id.remove_from_collected(self.amount_due)
            self.loan_id.borrower.branch.unpay(
                self.amount_due, self.loan_id.borrower.full_name())
            self.save()

    def apply_late_fee(self):
        print("Applying Late Fee")
        if self.loan_id.payment_method == "compound_interest":
            # Check if there are past due
            is_past_due = True if self.loan_id.past_due > Decimal(0) else False
            print("Is there a past due:", is_past_due)
            if is_past_due:
                # If there is a past due, just add the amount due and take the interest
                amount = self.loan_id.past_due + self.amount_due
                amount = (self.loan_id.late_fee_rate / Decimal('100')) * (
                    amount) + amount
                self.amount_due = amount
                self.loan_id.set_past_due(amount)
                self.save()
            else:
                # If there's no pass due and this is late, just add interest_rate
                # Also make this the new past due
                print("There's no pass due... adding interest")
                print("Amount due:", self.amount_due)
                amount = (self.loan_id.late_fee_rate / Decimal("100")) * (
                    self.amount_due) + self.amount_due
                print("Lat e fee applied", amount)
                self.amount_due = amount
                self.loan_id.set_past_due(amount)
                print(self.loan_id.past_due)
                self.save()
                self.loan_id.save()

        else:
            self.amount_due = (self.loan_id.late_fee_rate / Decimal("100")) * (
                self.amount_due) + self.amount_due
            self.save(update_fields=['amount_due'])

    def mark_late(self):
        """ Mark this payment as late """
        # Remove the amount due to expected
        # Apply the late fee to amount due
        # Add to expected
        # Increment borrow missed payments
        if self.loan_id.schedule == "daily" and self.is_late():
            # Just increment the missed payment on daily loans
            self.loan_id.borrower.increment_missed_payments()
        elif self.is_late():
            self.late_fee_applied = True
            self.loan_id.borrower.increment_missed_payments()
            self.loan_id.remove_from_expected(self.amount_due)
            self.apply_late_fee()
            self.save()
            self.loan_id.refresh_from_db()
            self.loan_id.add_to_expected(self.amount_due)

    def __unicode__(self):
        return

    def get_absolute_url(self):
        return reverse('loans:loan_update_payment', kwargs={'uuid': self.loan_id.uuid, 'pay_uuid': self.uuid})

    class Meta:
        ordering = ["due_date"]
