from django.db import models
from django.shortcuts import reverse

from lendr.utils import id_generator
from lendr.banks.models import Bank
# Create your models here.

class Profile(models.Model):
    title = models.CharField(max_length=500, blank=True, null=True)
    middle_inital = models.CharField(max_length=500, blank=True, null=True)
    birthday = models.CharField(max_length=500, blank=True, null=True)
    age = models.PositiveIntegerField(default=0)
    academic_background = models.CharField(max_length=500, blank=True, null=True)
    home_address = models.CharField(max_length=500, blank=True, null=True)
    residential_status = models.CharField(max_length=500, blank=True, null=True)
    length_of_residence = models.CharField(max_length=20, blank=True, null=True)
    permanent_address = models.CharField(max_length=500, blank=True, null=True)
    telephone_number = models.CharField(max_length=500, blank=True, null=True)

    type_of_employment = models.CharField(max_length=500, blank=True, null=True)
    monthly_income = models.CharField(max_length=500, blank=True, null=True)
    employer = models.CharField(max_length=500, blank=True, null=True)
    employer_address = models.CharField(max_length=500, blank=True, null=True)
    employer_phone = models.CharField(max_length=500, blank=True, null=True)
    employment_length = models.CharField(max_length=500, blank=True, null=True)
    employer_relationship = models.CharField(max_length=500, blank=True, null=True)

    reference_one_full_name = models.CharField(max_length=500, blank=True, null=True)
    reference_one_address = models.CharField(max_length=500, blank=True, null=True)
    reference_one_phone = models.CharField(max_length=500, blank=True, null=True)
    reference_one_relationship = models.CharField(max_length=500, blank=True, null=True)

    reference_two_full_name = models.CharField(max_length=500, blank=True, null=True)
    reference_two_address = models.CharField(max_length=500, blank=True, null=True)
    reference_two_phone = models.CharField(max_length=500, blank=True, null=True)
    reference_two_relationship = models.CharField(max_length=500, blank=True, null=True)

    spouse_full_name = models.CharField(max_length=500, blank=True, null=True)
    spouse_birthday = models.CharField(max_length=500, blank=True, null=True)
    spouse_employer = models.CharField(max_length=500, blank=True, null=True)
    spouse_employer_address = models.CharField(max_length=500, blank=True, null=True)
    spouse_employer_telephone = models.CharField(max_length=500, blank=True, null=True)
    spouse_job_title = models.CharField(max_length=500, blank=True, null=True)
    spouse_monthly_income = models.CharField(max_length=500, blank=True, null=True)
    spouse_telephone = models.CharField(max_length=500, blank=True, null=True)
    spouse_employment_length = models.CharField(max_length=500, blank=True, null=True)


    comaker_full_name = models.CharField(max_length=500, blank=True, null=True)
    comaker_address = models.CharField(max_length=500, blank=True, null=True)
    comaker_telephone = models.CharField(max_length=500, blank=True, null=True)
    comaker_relationship = models.CharField(max_length=500, blank=True, null=True)
    comaker_number_of_dependents = models.CharField(max_length=500, blank=True, null=True)
    comaker_job_title = models.CharField(max_length=500, blank=True, null=True)
    comaker_monthly_income = models.CharField(max_length=500, blank=True, null=True)


    class Meta:
        abstract = True


class Client(Profile):

    timestamp = models.DateTimeField(auto_now=True)
    first_name = models.CharField(max_length=255, blank=False, null=False)
    last_name = models.CharField(max_length=255, blank=False, null=False)
    uuid = models.CharField(max_length=22,
                            db_index=True, default=id_generator, editable=False)
    missed_payments = models.IntegerField(verbose_name="Missed Payments",
                                          default=0, blank=True, null=True)
    branch = models.ForeignKey(
        Bank, related_name='client_bank', on_delete=models.CASCADE, default=None)


    def get_absolute_url(self):
        return reverse('clients:client_details', kwargs={'uuid': self.uuid, 'tab': ""})

    def full_name(self):
        return "{}, {}".format(self.last_name, self.first_name)

    def increment_missed_payments(self):
        self.missed_payments += 1
        self.save(update_fields=['missed_payments'])

    def __str__(self):
        return

    def __unicode__(self):
        return

    class Meta:
        ordering = ["-timestamp"]

class Comments(models.Model):

    timestamp = models.DateTimeField(auto_now=True)
    note = models.TextField(default="")
    client = models.ForeignKey(Client, related_name='client_notes', on_delete=models.CASCADE, default=None)
    poster = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        ordering = ["-timestamp"]
