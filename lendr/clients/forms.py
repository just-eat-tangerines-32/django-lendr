from django import forms
from django.forms import ModelForm

from lendr.loans.models import Loan
from .models import Client


class NewLoanForm(ModelForm):
    class Meta:
        model = Loan
        fields = ['principal', 'interest_rate',
                  'term', 'schedule', 'processing_fee', 'notarial_fee', 'insurance_fee', 'grace_period', 'payment_method', 'late_fee_rate']
        widgets = {
            'principal': forms.TextInput(attrs={'class': 'input'}),
            'interest_rate': forms.TextInput(attrs={'class': 'input'}),
            'notarial_fee': forms.TextInput(attrs={'class': 'input'}),
            'insurance_fee': forms.TextInput(attrs={'class': 'input'}),
            'processing_fee': forms.TextInput(attrs={'class': 'input'}),
            'term': forms.TextInput(attrs={'class': 'input'}),
            'grace_period': forms.TextInput(attrs={'class': 'input'}),
            'late_fee_rate': forms.TextInput(attrs={'class': 'input'}),
        }

class EditClientProfileForm(ModelForm):
    class Meta:
        model = Client
        exclude = ['pk', 'branch']
