from django.shortcuts import render, get_object_or_404, reverse
from django.views.generic import DetailView, UpdateView, CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.urls import reverse_lazy
from lendr.utils import GetClientLoans, GetClientComments
from django.http import HttpResponseRedirect

from .models import Client, Comments
from .forms import NewLoanForm, EditClientProfileForm
from lendr.loans.models import Loan
from lendr.calculator.forms import LoanCalculatorForm
# Create your views here.


class AllClientsView(LoginRequiredMixin, ListView):

    model = Client
    template_name = 'clients/all.html'
    paginate_by = 25


class ClientDeleteView(PermissionRequiredMixin, SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    permission_required = "clients.delete_client"
    raise_exception = True
    redirect_field_name = 'DashRedirect'
    model = Client
    template_name = 'clients/delete_confirm.html'
    success_message = 'Client successfully deleted!'

    def get_success_url(self):
        bank = self.object.branch.uuid
        return reverse_lazy('banks:bank_details', kwargs={'uuid': bank})

    def get_object(self):
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return client

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ClientDeleteView, self).delete(request, *args, **kwargs)


class ClientEditProfile(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = "clients.change_client"
    raise_exception = True
    model = Client
    form_class = EditClientProfileForm
    template_name = 'clients/client_edit_profile.html'

    def get_success_url(self):
        msg = "Profile succesfully updated"
        messages.success(self.request, msg)
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return reverse('clients:client_details', kwargs={'uuid': client.uuid, 'tab': 'profile'})

    def get_object(self):
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return client

class ClientAddComment(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = ("clients.add_notes", "clients.add_comments")
    raise_exception = True
    model = Comments
    fields = ['note']
    template_name = 'clients/add_note.html'

    def get_success_url(self):
        msg = "Comment succesfully added"
        messages.success(self.request, msg)
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return reverse('clients:client_details', kwargs={'uuid': client.uuid, 'tab': 'comments'})

    def form_valid(self, form):
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        form.instance.client =  client
        form.instance.poster =  self.request.user
        self.object = form.save()
        # do something with self.object
        # remember the import: from django.http import HttpResponseRedirect
        return HttpResponseRedirect(self.get_success_url())

    def get_object(self):
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return client


class NewClientLoan(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = ("loans.add_loan")
    raise_exception = True
    model = Loan
    form_class = NewLoanForm

    def get_success_url(self):
        return reverse('loans:loan_details', kwargs={'uuid': self.object.uuid})
        # return reverse('loans:loan_details', kwargs={'uuid': self.kwargs['uuid']})

    def form_valid(self, form):
        form.instance.borrower = Client.objects.get(uuid=self.kwargs['uuid'])
        msg = "Loan was succesfully created!"
        messages.success(self.request, msg)
        return super(NewClientLoan, self).form_valid(form)


class ClientDetailView(LoginRequiredMixin, DetailView):
    model = Client

    def get_object(self):
        client = get_object_or_404(Client, uuid=self.kwargs['uuid'])
        return client

    def get_context_data(self, **kwargs):
        context = super(ClientDetailView, self).get_context_data(**kwargs)
        loans = GetClientLoans(self.object.uuid)
        notes = GetClientComments(self.object.uuid)
        context["tab"] = self.kwargs['tab'] if self.kwargs['tab'] else "loan"
        context["loans"] = loans
        context["notes"] = notes
        return context
