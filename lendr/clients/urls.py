from django.conf.urls import url

from . import views
urlpatterns = [
    url(
        regex=r'^client/(?P<uuid>[0-9A-Za-z]{9,})/new_loan$',
        view=views.NewClientLoan.as_view(),
        name='client_new_loan',
    ),
    url(
        regex=r'^client/(?P<uuid>[0-9A-Za-z]{9,})/add_note$',
        view=views.ClientAddComment.as_view(),
        name='client_add_note',
    ),
    url(
        regex=r'^client/(?P<uuid>[0-9A-Za-z]{9,})/delete$',
        view=views.ClientDeleteView.as_view(),
        name='client_delete',
    ),
    url(
        regex=r'^$',
        view=views.AllClientsView.as_view(),
        name='all',
    ),
    url(
        regex=r'^client/(?P<uuid>[0-9A-Za-z]{9,})/edit_profile$',
        view=views.ClientEditProfile.as_view(),
        name='client_edit_profile',
    ),
    url(
        regex=r'^client/(?P<uuid>[0-9A-Za-z]{9,})/(?P<tab>.*)$',
        view=views.ClientDetailView.as_view(),
        name='client_details',
    ),
]
