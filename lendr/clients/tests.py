from decimal import Decimal

from django.test import TestCase

from lendr.banks.models import Bank
from .models import Client

# Create your tests here.


class ClientTests(TestCase):

    def setUp(self):
        Bank.objects.create(
            name="Montreal", balance=Decimal(100), currency="CAD")
        Client.objects.create(
            first_name="Adam", last_name="Brown", branch=Bank.objects.first())
        Client.objects.create(
            first_name="Eva", last_name="Brown", branch=Bank.objects.first())

    def test_objects_created(self):
        self.assertEqual(Client.objects.all().count(), 2)

    def test_increment_missed_payments(self):
        c = Client.objects.first()
        c.increment_missed_payments()
        self.assertEqual(c.missed_payments, 1)

    def test_get_bank_clients(self):
        c = Client.objects.filter(
            branch__uuid=Bank.objects.first().uuid).count()
        self.assertEqual(c, 2)
