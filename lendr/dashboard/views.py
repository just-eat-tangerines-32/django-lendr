from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from lendr.loans.models import Payment
from lendr.banks.models import Bank, Transaction
from lendr.clients.models import Client


# Create your views here.


def DashRedirect(request):
    """
        The root page:
        If the user is logged in, it will redirect to the actual Dashboard
        If the user is not logged in, it will redirect to the login page
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('dashboard'))
    else:
        msg = "You need to be logged in to proceed"
        messages.info(request, msg)
        return HttpResponseRedirect(reverse('login'))


@login_required(redirect_field_name='DashRedirect')
def Dashboard(request):
    banks = Bank.objects.all()
    transactions = Transaction.objects.all()[:10]
    payments = Payment.objects.filter(paid=False)[:10]
    clients = Client.objects.all()[:10]
    context = {'banks': banks,
               'transactions': transactions, 'clients': clients, 'payments': payments}
    return render(request, "dashboard/index.html", context)
