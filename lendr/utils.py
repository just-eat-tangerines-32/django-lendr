

def id_generator(size=12):
    from django.utils.crypto import get_random_string
    return get_random_string(size)

def GetTotalBalance():
    """ Return the total balance of all banks; does not account for different currencies """
    from lendr.banks.models import Bank
    from decimal import Decimal
    out = Decimal("0")
    for bank in Bank.objects.all():
        out += bank.balance
    return out


def GetLoanPayments(uuid):
    """ Get the payments for a given loan """
    from lendr.loans.models import Payment
    payments = Payment.objects.filter(
        loan_id__uuid=uuid)
    return payments


def GetClientLoans(uuid):
    """ Get the clients loans """
    from lendr.loans.models import Loan
    loans = Loan.objects.filter(borrower__uuid=uuid)
    return loans

def GetClientComments(uuid):
    """ Get the clients notes """
    from lendr.clients.models import Comments
    notes = Comments.objects.filter(client__uuid=uuid)
    return notes


def GetBankClients(uuid):
    """ Get Clients from a given branch """
    from lendr.clients.models import Client
    clients = Client.objects.filter(branch__uuid=uuid)
    return clients


def GetBankLoans(uuid):
    from lendr.loans.models import Loan
    """ Get Loans from a given branch """
    loans = Loan.objects.filter(borrower__branch__uuid=uuid)
    return loans


def GetBankPayments(uuid):
    """ Get Payments from a given branch """
    from lendr.loans.models import Payment
    bank_payments = Payment.objects.filter(
        loan_id__borrower__branch__uuid=uuid)
    return bank_payments
